# -*- coding: utf-8 -*-
"""
Created on Sun Mar 15 21:21:39 2015

@author: dkadish

"""

import pygame

from .state import State, StateMixin

class LightState(State):

    def __init__(self, name, update_function=None, hsl=(0,0,0), ranges=((0,0),(0,0),(0,0)), freqs=[0,0,0]):
        '''
        Parameters
        ----------
        hsl : list
            The target HSL values
        ranges : list of lists
            The amount higher and lower that the HSL target can go
        freqs : list
            The frequencies of the switches from lowest to highest in Hz
        '''
        if update_function == None:
            update_function = self._update
        super(LightState, self).__init__(name, update_function)
        
        self.hsl = hsl
        self.ranges = ranges
        self.freqs = freqs
        
        self.values = hsl
        
        self._dir_plus = None # Tells wheather it is going forward or backwards
        
    def _update(*args, **kwargs):
        #TODO: Two different modes. fade to range, and then vary. FOR EACH VARIABLE.      
        
        # Check if we are in the range
        if self._dir_plus == None:
            self._dir_plus = [self.values[i] < self.hsl[i] for i in xrange(3)]
            
        if map(lambda i: self.hsl[i] >= self.ranges[i][0] and self.hsl[i] =< self.ranges[i][1] ):
            
                

class LightSprite(object):            
    '''
    LeafSprites are defined by a set of numbers.
    
    S - the number of states in the state machine
    P_m - the number of points for modifier 'm'
    
    transition_matrix = [SxS] matrix
    modifier matrices = [P_m x S] matrix
    modifiers points = [P_m x 1] matrix
    '''
    pass

class RGBSprite(pygame.sprite.Sprite, StateMixin):
    
    def __init__(self, connections, boards, pins, states=None, **kwargs):
        '''
        
        Parameters
        boards : str or array
            
        pins : array
            Pin numbers of [r, g, b] LEDs
        '''