# -*- coding: utf-8 -*-
"""
Created on Tue Feb 10 19:39:31 2015

@author: dkadish
"""

import collections, copy
from inspyred.ec import EvolutionaryComputation, Bounder, Individual

from . import StateSprite

class Evolutionary(EvolutionaryComputation):
    '''
    Can I simply run the generator algorithm on the first run, terminate after
    every generation, and then re-run the evolution?
    
    Parameters
    ----------
    random : 
    generator : 
    evaluator : 
    maximize : boolean
        Is the evaluator trying to maximize or minimize?
    bounder : 
    '''
    
    def __init__(self, random, generator, evaluator, maximize=True, bounder=None, *args, **kwargs):
        super(Evolutionary, self).__init__(random)
        
        self._kwargs = args
        self._kwargs['_ec'] = self
        
        if bounder is None:
            bounder = Bounder()
        
        self.termination_cause = None
        self.generator = generator
        self.evaluator = evaluator
        self.bounder = bounder
        self.maximize = maximize
        self.population = []
        self.archive = []
        
        self.num_generations = -1
        
    def _should_terminate(self, pop, ng, ne):
        '''
        pop: population as a list
        ng: number of generations
        ne: number of evaluations
        '''
    
    def next(self):
        self.evolve()
        return self
        
    def initial_population(self, pop_size=10, seeds=None):
        if seeds is None:
            seeds = []
            
        # Create the initial population.
        if not isinstance(seeds, collections.Sequence):
            seeds = [seeds]
        initial_cs = copy.copy(seeds)
        num_generated = max(pop_size - len(seeds), 0)
        i = 0
        self.logger.debug('generating initial population')
        while i < num_generated:
            cs = self.generator(random=self._random, args=self._kwargs)
            initial_cs.append(cs)
            i += 1
        
        self.cs = initial_cs
        
    def evolve(self):
        '''
        1. Check fitness
        2. Handle observers
        3. Select individuals
        4. Evaluate offspring
        5. Replace individuals
        6. Migrate individuals (NO)
        7. Archive individuals
        8. Observe
        9. Select
        10. Vary
        '''
        if self.num_generations < 0:
            self.__initial_evolve(self.cs)
        else:
            self.__subsequent_evolve(self.cs, self.parents)
            
    def __initial_evolve(self, initial_cs, parents=None):
        initial_fit = self.evaluator(candidates=initial_cs, args=self._kwargs)
        
        for cs, fit in zip(initial_cs, initial_fit):
            if fit is not None:
                ind = Individual(cs, maximize=self.maximize)
                ind.fitness = fit
                self.population.append(ind)
            else:
                self.logger.warning('excluding candidate {0} because fitness received as None'.format(cs))
        self.logger.debug('population size is now {0}'.format(len(self.population)))
        
        self.num_evaluations = len(initial_fit)
        self.num_generations = 0
        
        self.logger.debug('archiving initial population')
        self.archive = self.archiver(random=self._random, population=list(self.population), archive=list(self.archive), args=self._kwargs)
        self.logger.debug('archive size is now {0}'.format(len(self.archive)))
        self.logger.debug('population size is now {0}'.format(len(self.population)))
                
        if isinstance(self.observer, collections.Iterable):
            for obs in self.observer:
                self.logger.debug('observation using {0} at generation {1} and evaluation {2}'.format(obs.__name__, self.num_generations, self.num_evaluations))
                obs(population=list(self.population), num_generations=self.num_generations, num_evaluations=self.num_evaluations, args=self._kwargs)
        else:
            self.logger.debug('observation using {0} at generation {1} and evaluation {2}'.format(self.observer.__name__, self.num_generations, self.num_evaluations))
            self.observer(population=list(self.population), num_generations=self.num_generations, num_evaluations=self.num_evaluations, args=self._kwargs)

        # Select individuals.
        self.logger.debug('selection using {0} at generation {1} and evaluation {2}'.format(self.selector.__name__, self.num_generations, self.num_evaluations))
        parents = self.selector(random=self._random, population=list(self.population), args=self._kwargs)
        self.logger.debug('selected {0} candidates'.format(len(parents)))
        parent_cs = [copy.deepcopy(i.candidate) for i in parents]
        offspring_cs = parent_cs
        
        if isinstance(self.variator, collections.Iterable):
            for op in self.variator:
                self.logger.debug('variation using {0} at generation {1} and evaluation {2}'.format(op.__name__, self.num_generations, self.num_evaluations))
                offspring_cs = op(random=self._random, candidates=offspring_cs, args=self._kwargs)
        else:
            self.logger.debug('variation using {0} at generation {1} and evaluation {2}'.format(self.variator.__name__, self.num_generations, self.num_evaluations))
            offspring_cs = self.variator(random=self._random, candidates=offspring_cs, args=self._kwargs)
        self.logger.debug('created {0} offspring'.format(len(offspring_cs)))
        
        self.cs = offspring_cs
        self.parents = parents
    
    def __subsequent_evolve(self, offspring_cs, parents):
  
        # Evaluate offspring.
        self.logger.debug('evaluation using {0} at generation {1} and evaluation {2}'.format(self.evaluator.__name__, self.num_generations, self.num_evaluations))
        offspring_fit = self.evaluator(candidates=offspring_cs, args=self._kwargs)
        offspring = []
        for cs, fit in zip(offspring_cs, offspring_fit):
            if fit is not None:
                off = Individual(cs, maximize=self.maximize)
                off.fitness = fit
                offspring.append(off)
            else:
                self.logger.warning('excluding candidate {0} because fitness received as None'.format(cs))
        self.num_evaluations += len(offspring_fit)        

        # Replace individuals.
        self.logger.debug('replacement using {0} at generation {1} and evaluation {2}'.format(self.replacer.__name__, self.num_generations, self.num_evaluations))
        self.population = self.replacer(random=self._random, population=self.population, parents=parents, offspring=offspring, args=self._kwargs)
        self.logger.debug('population size is now {0}'.format(len(self.population)))
        
        # Migrate individuals.
        self.logger.debug('migration using {0} at generation {1} and evaluation {2}'.format(self.migrator.__name__, self.num_generations, self.num_evaluations))
        self.population = self.migrator(random=self._random, population=self.population, args=self._kwargs)
        self.logger.debug('population size is now {0}'.format(len(self.population)))
        
        # Archive individuals.
        self.logger.debug('archival using {0} at generation {1} and evaluation {2}'.format(self.archiver.__name__, self.num_generations, self.num_evaluations))
        self.archive = self.archiver(random=self._random, archive=self.archive, population=list(self.population), args=self._kwargs)
        self.logger.debug('archive size is now {0}'.format(len(self.archive)))
        self.logger.debug('population size is now {0}'.format(len(self.population)))
        
        self.num_generations += 1
        if isinstance(self.observer, collections.Iterable):
            for obs in self.observer:
                self.logger.debug('observation using {0} at generation {1} and evaluation {2}'.format(obs.__name__, self.num_generations, self.num_evaluations))
                obs(population=list(self.population), num_generations=self.num_generations, num_evaluations=self.num_evaluations, args=self._kwargs)
        else:
            self.logger.debug('observation using {0} at generation {1} and evaluation {2}'.format(self.observer.__name__, self.num_generations, self.num_evaluations))
            self.observer(population=list(self.population), num_generations=self.num_generations, num_evaluations=self.num_evaluations, args=self._kwargs)
        
        # Select individuals.
        self.logger.debug('selection using {0} at generation {1} and evaluation {2}'.format(self.selector.__name__, self.num_generations, self.num_evaluations))
        parents = self.selector(random=self._random, population=list(self.population), args=self._kwargs)
        self.logger.debug('selected {0} candidates'.format(len(parents)))
        parent_cs = [copy.deepcopy(i.candidate) for i in parents]
        offspring_cs = parent_cs
        
        if isinstance(self.variator, collections.Iterable):
            for op in self.variator:
                self.logger.debug('variation using {0} at generation {1} and evaluation {2}'.format(op.__name__, self.num_generations, self.num_evaluations))
                offspring_cs = op(random=self._random, candidates=offspring_cs, args=self._kwargs)
        else:
            self.logger.debug('variation using {0} at generation {1} and evaluation {2}'.format(self.variator.__name__, self.num_generations, self.num_evaluations))
            offspring_cs = self.variator(random=self._random, candidates=offspring_cs, args=self._kwargs)
        self.logger.debug('created {0} offspring'.format(len(offspring_cs)))
        
        self.cs = offspring_cs
        self.parents = parents
        
class EvolutionarySprite(StateSprite, Evolutionary):
    
    def __init__(self, connections, board, pin, random, generator, evaluator,
                 states=None, maximize=True, bounder=None, *args, **kwargs):
        super(EvolutionarySprite, self).__init__(connections=connections, board=board,
                pin=pin, states=states, random=random, generator=generator,
                evaluator=evaluator, maximize=maximize, bounder=bounder,
                *args, **kwargs)
        Evolutionary.__init__(random, generator, evaluator, maximize, bounder,
                              *args, **kwargs)
                
        self._genome_to_properties(self.initial_population())
        
    def generator(random, args):
        '''Generates new candidate genomes for the population.
        '''
        pass
    
    def evaluator(candidates, args):
        pass
    
    def _genome_to_properties(self, cs):
        pass