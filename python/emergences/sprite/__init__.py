#!/usr/bin/python
# coding: utf-8

import os
PATH = os.path.dirname(os.path.abspath(__file__))
for i in range(2):
    PATH = os.path.split(PATH)[0]

import pygame, hashlib
from datetime import datetime

import numpy as np
import scipy
import scipy.interpolate

import joblib

from .state import StateMixin, State, StateMachine
from ..sensor import SensorGroup

class PWMSprite(pygame.sprite.Sprite):
    '''Base class for a sprite that controls the PWM pins of an Arduino.
    
    parent: the parent PyGame object that this sprite belonds to
    board: the board id that the sprite is on
    pin: the pin number on the Arduino that controls the sprite
    states: the State Machine object that holds the sprite's state
    '''
    
    def __init__(self, connections, board, pin, states=None, **kwargs):
        super(PWMSprite, self).__init__()
        
        self.connections = connections
        self.connections.add(self, board)
        self.pin = pin
        self.value = 0
        
        self.update_args = []
        self.update_kwargs = {}

    def update(self, *args):
        super(PWMSprite, self).update(*args)        
                
    def on_render(self):
        if self.connection: #TODO: I dont think 
            self.writeSerial()
            
    def on_cleanup(self):
        self.connection.write('PWM %d %d\n' %(self.pin, 0))
            
    def readSerial(self, value, cast=None):
        self.connection.write("%s\n" %(value))
        line = self.connection.readline()
        
        if cast:
            return cast(line.strip())
            
        return line.strip()
        
    def writeSerial(self):
        #print 'PWM %d %d' %(self.pin, self.value)
        self.connection.write("PWM %d %d\n" %(self.pin, self.value))
        
    @property
    def connection(self):
        return self.connections.connection(self)
        
    @property
    def name(self):
        return '%s, %d' %(self.connections.objects[self],self.pin)
        
class StateSprite(PWMSprite, StateMixin):
    '''Base object for a sprite that uses a state machine to track its state
    
    The contexts for the states are stored using the querystring for the sensor
    as a name.
    '''
    
    def __init__(self, parent, board, pin, states=None, sensors=None, **kwargs):
        super(StateSprite, self).__init__(parent, board, pin, **kwargs)
        StateMixin.__init__(self, states, sensors)
        print 'SENSORS: ', sensors
    
    def update(self, *args):
        super(StateSprite,self).update(*args)
        StateMixin.update(self, *args)
#        if StateMixin.update(self, *args) != None:
#            print 'New State: %d, %s' %(self.pin, self.states.state.name)
        
    def play_from_pchip(self, pchip_filename):
        pchip_filename = os.path.join(PATH, pchip_filename)
        try:
            pchip = getattr(self, hashlib.md5(pchip_filename).hexdigest())
        except AttributeError as e:
            print e
            setattr(self, hashlib.md5(pchip_filename).hexdigest(),
                    joblib.load(pchip_filename))
            pchip = getattr(self, hashlib.md5(pchip_filename).hexdigest())
                    
        try:
            value = pchip(milliseconds(self.start_time), extrapolate=False)
            if np.isnan(value):
                del self.start_time
                return False
        except AttributeError:
            self.start_time = datetime.now()
            return True
        
        self.value = int(value)
        
        return True
        
    @property
    def S(self):
        return len(self.states)
        
    @property
    def P_m(self):
        return len(self.mod_pts) #TODO: should also work for self.mod_arrays
        
    def play_current_state_from_pchip(self, *args, **kwargs):
        return self.play_from_pchip(self.states.state.name)
        
    @classmethod
    def from_json(cls, app, json_obj):
        board = json_obj['board']
        pin = json_obj['pin']
        statefiles = json_obj['statefiles']
        transformation = json_obj['transformation']
        mod_arrays_json = json_obj['mod_arrays']
        mod_pts_json = json_obj['mod_pts']
        
        try:
            sensors_json = json_obj['sensors']
        except KeyError:
            sensors_json = []
            
        matrix = np.array(transformation).astype(np.float)
        
        mod_arrays = {}
        for m in mod_arrays_json:
            mod_arrays[m] = np.array(mod_arrays_json[m]).astype(np.float)
            
        mod_pts = {}
        for m in mod_pts_json:
            mod_pts[m] = np.array(mod_pts_json[m]).astype(np.float)
            
        modifiers = {}
        for m in mod_arrays:
            modifiers[m] = scipy.interpolate.PchipInterpolator(mod_pts[m], mod_arrays[m])
        
        sensors = []
        for s in sensors_json:
            for s_app in app.sensors:
                if s_app.name == s:
                    sensors.append(s_app)
        
        # Create the object with no states
        clsobj = cls(app.connections, board, pin, sensors=sensors)
        
        # Fix the statemachine update functions
        states = [State('null', cls.null)]
        for s in statefiles:
            state = State(s, clsobj.play_current_state_from_pchip)#getattr(clsobj, func_name))
            states.append(state)
            
        state_machine = StateMachine(states, transition_matrix=matrix,
                                   modifiers=modifiers)
        clsobj.states = state_machine
        
        return clsobj
        
class LightingGroup(pygame.sprite.Group):
    pass
    
class LeafGroup(pygame.sprite.Group):
    pass

def milliseconds(since):
    td = datetime.now() - since
    return int(td.total_seconds() * 1000)

if __name__ == '__main__':
    pass
