# -*- coding: utf-8 -*-
"""
Created on Sun Mar  8 14:05:31 2015

@author: dkadish
"""

import pygame

from .state import StateMixin

class AudioSprite(pygame.sprite.Sprite, StateMixin):
    '''Sprite that plays sounds depending on which state it is in.
    '''
    
    def __init__(self, sounds, states, channel_number, *groups):
        '''
            
        Parameters
        ----------
        sounds : ??
            The set of sounds that may be played from the sprite.
        channel_number : int
            Either 0 or 1 (left or right) for which side this AudioSprite
            represents.
        '''
        super(AudioSprite, self).__init__(*groups)
        StateMixin.__init__(states)
        
        self.sounds = sounds
        self.channel_number = channel_number
        
    def update(self, *args):
        super(AudioSprite, self).update(*args)
        
        # Has the state changed
        if StateMixin.update(*args):
            pass