# -*- coding: utf-8 -*-
"""
Created on Tue Feb 10 20:46:33 2015

@author: dkadish
"""
import numpy as np
import scipy
import scipy.interpolate

from . import StateSprite
#from .evolve import EvolutionarySprite
from .state import State, StateMachine
from ..sensor.tsl2561 import TSL2561
                        
class LeafSprite(StateSprite):            
    '''
    LeafSprites are defined by a set of numbers.
    
    S - the number of states in the state machine
    P_m - the number of points for modifier 'm'
    
    transition_matrix = [SxS] matrix
    modifier matrices = [P_m x S] matrix
    modifiers points = [P_m x 1] matrix
    '''
    pass

class WhiteLeaf(LeafSprite):
    matrix = np.array([[5, 3, 3, 3, 0, 0],
                       [3, 5, 5, 5, 0, 0],
                       [3, 5, 5, 5, 0, 0],
                       [0, 0, 0, 0, 1, 0],
                       [0, 0, 0, 0, 5, 1],
                       [3, 5, 5, 5, 0, 0]])
    
    BREATHE = 'csv/white-bi-big-breathe.pchip'
    HYPER = 'csv/white-bi-big-hyper.pchip'
    UP = 'csv/white-bi-big-up.pchip'
    STAY = 'csv/white-bi-big-stay.pchip'
    DOWN = 'csv/white-bi-big-down.pchip'
    
    def __init__(self, connections, board, pin, states=None):
        super(LeafSprite, self).__init__(connections, board, pin, states)
        
        # Initialize States
        still = State('still', self.null)
        breathe = State('breathe', self.breathe)
        hyper = State('hyper', self.hyper)
        up = State('up', self.up)
        stay = State('stay', self.stay)
        down = State('down', self.down)
        
#        print 'WHITE LEAF CONNECTION: ', self.connection
        
        self.sensors.add(TSL2561(self.connection))
        
#        self.state_connections = np.array([[1,1,1,1,0,0],
#                                     [1,1,1,1,0,0],
#                                     [1,1,1,1,0,0],
#                                     [0,0,0,0,1,0],
#                                     [0,0,0,0,1,1],
#                                     [1,1,1,1,0,0]])

        states = [still, breathe, hyper, up, stay, down]
        matrix = np.array(self.matrix).astype(np.float)
        
        mod_arrays = {}
        mod_arrays[self.sensors.sprites()[0].name] = np.array([[1.0, 0.2, 0.2, 0.4, 0.8, 0.4],
                                       [0.5, 0.8, 0.3, 0.6, 0.6, 0.5],
                                       [0.2, 0.5, 1, 0.5, 0.2, 0.5]])
        mod_pts = {}
        mod_pts[self.sensors.sprites()[0].name] = np.array([0.0, 100, 1000])
        
        # Create interpolated functions for the modifiers
        modifiers = {}
        for m in mod_arrays:
            modifiers[m] = scipy.interpolate.PchipInterpolator(mod_pts[m], mod_arrays[m])
        
        self.states = StateMachine(states, transition_matrix=matrix,
                                   starting_state=still, modifiers=modifiers)
                
    def generator(self, random, args):
        n_genes = self.S**2 + (self.S + 1)  * self.P_m
        rand_array = []
        for i in xrange(n_genes):
            rand_array.append(random.uniform(0, 1))
            
        # Morph the self.state_connections array and multiply by the candidate
        return None
            
    def evaluator(self, candidates, args):
        pass
    
    def _genome_to_properties(self, cs):
        pass
    
    ## State Definitions
    def breathe(self, *args, **kwargs):
        '''Update the value when breathing'''
        return self.play_from_pchip(self.BREATHE)
        
    def hyper(self, *args, **kwargs):
        return self.play_from_pchip(self.HYPER)
        
    def up(self, *args, **kwargs):
        return self.play_from_pchip(self.UP)
        
    def stay(self, *args, **kwargs):
        return self.play_from_pchip(self.STAY)
        
    def down(self, *args, **kwargs):
        return self.play_from_pchip(self.DOWN)
        
   