# -*- coding: utf-8 -*-
"""
Created on Sat Feb  7 16:11:20 2015

@author: dkadish
"""

import numpy as np
from ..sensor import SensorGroup

class State(object):
    '''A state of an object in the show'''
    
    def __init__(self, name, update_function):
        '''The initialization function'''
        self.name = name
        self.update_function = update_function
        
    def update(self, *args, **kwargs):
        '''Run the update function for that state'''
        return self.update_function(*args, **kwargs)
        
class StateMachine(object):
    '''A finite state machine
    
    >>> a = State('a', None)
    >>> b = State('b', None)
    >>> c = State('c', None)

    >>> matrix = np.array([[0,1,2],[3,4,5],[6,7,8]]).astype(np.float)
    >>> sm = StateMachine([a, b, c], transition_matrix = matrix)
    '''
    
    def __init__(self, states, transition_matrix=None, is_symmetric=False,
                 starting_state=None, modifiers={}):
        '''
        Parameters
        ----------
        
        modifiers : dict of INTERPOLATED FUNCTIONS
            a dictionary of Nx1 arrays (N is the number of states) of
            the modification to the transition matrix given different
            stimuli.
                   e.g. {'light': np.array([1, 0.5, 0,25])}
        '''
        self._trans = None                     
                     
        self.states = states
        self.transition_matrix = transition_matrix
        self.is_symmetric = is_symmetric
        self.modifiers = modifiers
        self._context = {}
        
        # Input Sanity Checks
        for m in self.modifiers:
            if self.modifiers[m](0).size != len(self.states):
                raise AttributeError('Size of modifier %s does not match number of states (%d)' %(m, len(self.states)))
            
            self._context[m] = 0.0
        
        self._state = starting_state
        if self.state == None:
            self._state = np.random.choice(self.states)
    
    def __iter__(self):
        return self
    
    def next(self):
        '''Get the next state'''
        next_state = self.get_next_state()
        self._state = next_state
        return self.state
        
    def get_next_state(self):
        '''Calculate the next state of the state machine'''
        base_probabilities = self.transition_matrix[self.index, :]
        #print 'Probabilities: ', base_probabilities, self.context_multipliers
        adjusted_probabilities = reduce(lambda x, y: x*y, [base_probabilities] + self.context_multipliers.values())
        #print 'Probabilities: ', base_probabilities, self.context_multipliers, adjusted_probabilities
        adjusted_probabilities[adjusted_probabilities < 0] = 0
	probabilities = normalize(adjusted_probabilities)
#        print self.context_multipliers, adjusted_probabilities, probabilities
        next_state = np.random.choice(self.states, p=probabilities)
        return next_state
    
    @property
    def transition_matrix(self):
        '''Get the transition matrix'''
        return self._trans
        
    @transition_matrix.setter
    def transition_matrix(self, mat):
        '''Turn into a float and then set'''
        self._trans = np.array(mat).astype(np.float)
        
    @property
    def state(self):
        '''Get the current state.'''
        return self._state
        
    @property
    def index(self):
        '''Get the index of the current state.'''
        return self.states.index(self.state)
        
    @property
    def context(self):
        return self._context
        
    @context.setter
    def context(self, c):
        if type(c) != dict:
            raise AttributeError('Context must be a dict')
        if len(c) != len(self.context):
            raise AttributeError('Context must be the same length as the contextual multipliers')
            
        self.context = c
        
    @property
    def context_multipliers(self):
        '''Multiply the contextual matrices by the actual context'''
        mods = {}
        for m in self.modifiers:
            #print 'Context: ', m, self.context[m]
            mods[m] = self.modifiers[m](self.context[m])
#            print 'Context %s: %s' %(m, self.context[m])
#            print 'Modifiers %s: %s' %(m, str(mods[m]))
        
        return mods
    
    def set_transition_probability(self, from_state, to_state, probability):
        '''Set the transition probability for a pair of states.'''
        from_index = self.states.index(from_state)
        to_index = self.states.index(to_state)
        self.transition_matrix[from_index, to_index] = probability

def normalize(mat, axis=0):
    return (mat.T/np.sum(mat, axis=axis)).T
    
if __name__ == "__main__":
    import doctest
    
    a = State('a', None)
    b = State('b', None)
    c = State('c', None)

    matrix = np.array([[0, 1, 2], [3, 4, 5], [6, 7, 8]]).astype(np.float)

    doctest.testmod(extraglobs={'sm': StateMachine([a, b, c], transition_matrix=matrix)})
    
class StateMixin(object):
    '''A mixin to add statefulness to sprites.
    
    Also adds sensors and context to sprites.    
    '''
    N_NULL = 25
    
    def __init__(self, states=None, sensors=None):
        super(StateMixin, self).__init__()
        self.states = states
        
        self.sensors = SensorGroup()
        if sensors != None:
            for s in sensors:
                self.sensors.add(s)
        
    def update(self, *args):
        self._sensor_update(*args)
        
        # If the current state is at the end.
        state_running = self.states.state.update(self, *self.update_args, **self.update_kwargs)
        if state_running == False:
            self.states.next()
            print 'Next State: %s | %s' %(str(self), self.states.state.name)
            return self.states.state
        
        return None
        
    def _sensor_update(self, *args):
        # Update Sensor Group        
        self.sensors.update(*args)
        
        # Update state contexts
        for sensor in self.sensors:
            self.states.context[sensor.name] = sensor.read()
#            print 'Sensor Reading: ', self.states.context[sensor.name]
        
            
    def null(self, *args, **kwargs):
        try:        
            # If it is less than zero, start a new counter
            if self.still_counter < 0:
                self.still_counter = self.N_NULL
        except AttributeError:
            self.still_counter = self.N_NULL
            
        self.still_counter -= 1
        self.value = 0

        # If it is NOW less than zero, stop counting.        
        if self.still_counter < 0:
            return False
            
        return True
