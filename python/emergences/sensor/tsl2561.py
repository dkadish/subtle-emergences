# -*- coding: utf-8 -*-
"""
Created on Tue Feb 17 10:36:27 2015

@author: dkadish
"""

from . import Sensor

class TSL2561(Sensor):
    
    def __init__(self, connection):
        super(TSL2561, self).__init__(connection, query_string='LUM\n', value_type=float)