'''Sensor package contains mixins for different types of sensors.

'''
import time

import numpy as np
import pygame

DELAY_TIME = 10

class Sensor(pygame.sprite.Sprite):
    '''Base class for sensors.
    '''
    
    def __init__(self, connections, board, query_string, name=None, aggregation_method=np.average, value_type=int):
        '''
        Parameters
        ----------
        connection : serial.Serial
            The serial connection to the sensor's interface.
        query_string : str
            The string to query the sensor's value via the serial connection.
        aggregation_method : function
            The method of        self.connections.add(self, board)
 aggregating multiple values (the default is np.average)
        value_type : type
            The type of value that is expected (int, float, str)
        '''
        super(Sensor, self).__init__()
        
        self.connections = connections
        self.connections.add(self, board)
        self.query_string = query_string
        self.aggregation_method = aggregation_method
        self.value_type = value_type
        self.name = name
        
        #TODO: hack. this will not be true if query string is allowed to be None
        if self.name == None:
            self.name = self.query_string
        
        self.values = []
        
    def update(self):
        '''Update the sensor readings
        '''
        self._query_sensor()
        
    def read(self):
        '''Aggregate and read the current value
        '''
        value = self.aggregation_method(self.values)
        #print 'Sensor Values: ', value, self.values
        self.values = []
        
        try:
            if np.isnan(value):
                raise ValueError('READ ERROR: Value is not a number.')
        except ValueError as e:
#            print e
            value = 1
            
        return value
        
    def _query_sensor(self):
        '''Find the latest value from the sensor.
        '''
        try:
            self.connection.write('%s\n' %str(self.query_string))
            pygame.time.wait(DELAY_TIME) # delay to allow the arduino to respond.
            value = self.connection.readline()
            value.strip()
            
            if self.value_type:
                value = self.value_type(value)
                
            if np.isnan(value):
                raise ValueError('WARNING: Sensor read a NaN')
            else:
                self.values.append(value)   
        except AttributeError as e:
            #print 'QUERY ATTRIBUTE ERROR: %s | %s' %(self.query_string, e)
            pass
        except ValueError as e:
            print 'QUERY VALUE ERROR: %s | %s' %(self.query_string, e)
            
    @classmethod
    def from_json(cls, connections, json_obj, name=""):
        board = json_obj['board']
        query_string = json_obj['query_string']
        
        try:
            value_type_raw = json_obj['value_type']
        except KeyError:
            value_type_raw = 'int'
        
        if value_type_raw == 'int':
            value_type = int
        elif value_type_raw == 'float':
            value_type = float
        elif value_type_raw == 'str':
            value_type = str
            
        try:
            aggregation_method_raw = json_obj['aggregation_method']
        except KeyError:
            aggregation_method_raw = 'np.average'
            
        if aggregation_method_raw == 'np.average':
            aggregation_method = np.average
        
        # Create the object with no states
        clsobj = cls(connections, board, query_string, name, aggregation_method, value_type)
        
        return clsobj
    
    @property
    def connection(self):
        return self.connections.connection(self)
        
class SensorGroup(pygame.sprite.Group):
    
    def __init__(self, *sprites):
        if sprites != None:
            super(SensorGroup, self).__init__(*sprites)
        else: 
            super(SensorGroup, self).__init__()
        self._sprites_dict = {}
        for s in sprites:
            self._sprites_dict[s.name] = s
    
    def add(self, *sprites):
        super(SensorGroup, self).add(*sprites)
        
        for sprite in sprites:
            try:
                self._sprites_dict[sprite.name] = sprite
            except AttributeError:
                for s in sprite:
                    self._sprites_dict[s.name] = s
                
    #TODO Implement has, __in__, remove
        
    def get_by_name(self, name):
        return self._sprites_dict[name]
