import serial, time
from serial.tools import list_ports

FILTER_TERMS = ['Bluetooth', 'FireFly']
TIMEOUT = 0.01

def usable_ports():
    ports = list_ports.comports()
    
    return filter(lambda p: not contains_any(p[0], FILTER_TERMS), ports)

def contains_any(search_string, targets):
    for t in targets:
        if search_string.find(t) != -1:
            return True
            
    return False

def connect(connections):
    '''Find all non-connected serial ports and try to connect to them'''
    
    ports_in_use = map(lambda c: c.port, connections)
    disconnected_ports = filter(lambda cs: cs[0] not in ports_in_use, usable_ports())
    
    for port, name, info in disconnected_ports:
        try:
            connections.append(serial.Serial(port, 9600, timeout=TIMEOUT))
        except serial.SerialException as e:
            pass
            #print 'SERIAL EXCEPTION: ', e, connections
        except OSError as e:
            print 'OS ERROR: ', e
            
    return connections
    
def map_connections(connections, boards):
    '''Map the set of connections to a dict of boards.
    
    Asks for the board ID in order to map it properly to the set of boards
    that we are supposed to have. This should only be done for new boards.
    '''
    for conn in filter(lambda c: c not in boards.values(), connections): #TODO: Does this work?
        print 'CONNECTION TEST: ', conn.name
        try:
            # Ask the board to identify itself
            conn.write('ID\n')
            time.sleep(TIMEOUT*5)
            reply = conn.readline()
            
#            if reply != []: # If there has been a reply
            # Check is this port still connected to its board?
            board = filter(lambda b: boards[b] == conn, boards)
                    
            if board == []:
                # Which is it connected to?
                for bid in boards:
                    if reply.startswith(bid):
                        boards[bid] = conn
                            
        except serial.SerialException as e:
            conn.close()
            board = filter(lambda b: boards[b] == conn, boards)
            if board != []:
                boards[board[0]] = None
                
    # Get rid of closed connections
    connections = filter( lambda c: c.isOpen(), connections )
    
    return connections, boards

def main():
    import time
    
    conns = []
    brds = {'0': None, '1':None, '105': None}
    
    while True:
        '''Test the functionality of the functions.'''
            
        # Connect
        conns = connect(conns)
        
        # Read/write
        conns, brds = map_connections(conns, brds)
        
        
        print [conn.port for conn in conns]
        print brds
        
        time.sleep(1)
        
if __name__ == '__main__':
    main()
