import tempfile, serial, random

class ConnectionManager(object):
    
    def __init__(self, ids):
        super(ConnectionManager, self).__init__()
    
    def update(self):
        pass
    
    def connection(self, obj):
        pass
    
    def add(self, obj, board):
        pass
        
    def close(self):
        pass
    
class FileConnectionManager(ConnectionManager):
    
    def __init__(self, ids):
        super(FileConnectionManager, self).__init__(ids)
        
        self.connections = {}
        self.objects = {}
        
        for i in ids:
            self.connections[i] = FakeSerial(i)
        
    def update(self):
        pass
    
    def connection(self, obj):
        return self.connections[self.objects[obj]]
    
    def add(self, obj, board):
        self.objects[obj] = board
        
    def close(self):
        
        for conn in self.connections:
            conn.close()
            
class FakeSerial(serial.FileLike):
    
    def __init__(self, i):
        super(FakeSerial, self).__init__()
        
        self.id = i
        self.queue = []
        self.vars = {}
    
    def read(self):
        try:
            return top_pop(self.queue)
        except IndexError:
            return ''
    
    def write(self, w):
        if w == 'LUM\n':
            if w in self.vars:
                self.vars[w] += random.normalvariate(0,1)
            else:
                self.vars[w] = random.normalvariate(0,1)
            self.queue.append(str(self.vars[w]))
        if w == 'ID\n':
            return self.id
    
    def readline(self):
        return self.read()
    
def top_pop(q):
     q.reverse()
     a = q.pop()
     q.reverse()
     return a