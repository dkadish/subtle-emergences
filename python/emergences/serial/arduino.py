# -*- coding: utf-8 -*-
"""
Created on Tue Feb 24 15:05:28 2015

@author: dkadish
"""

from . import ConnectionManager
from .persistence import connect, map_connections

class ArduinoConnectionManager(ConnectionManager):
    '''Manages connections to multiple Arduinos for a project.
    '''
    
    def __init__(self, board_ids):
        super(ArduinoConnectionManager, self).__init__(board_ids)
        
        self.port_map = {}     
        self.connections = []
        self.objects = {}
        
        for b in board_ids:
            self.port_map[b] = None
            
        self.update()
    
    def update(self):
        self.connections = connect(self.connections)
        self.connections, self.boards = map_connections(self.connections, self.port_map)
    
    def connection(self, obj):
        return self.port_map[self.objects[obj]]
    
    def add(self, obj, board):
        self.objects[obj] = board
        
    def close(self):
        
        for conn in self.connections:
            conn.close()