# -*- coding: utf-8 -*-
"""
Created on Mon Apr 13 11:55:12 2015

@author: dkadish
"""

import sqlite3
from datetime import datetime

DB_NAME = 'subtle_emergences.db'
CREATE_STRING = 'CREATE TABLE IF NOT EXISTS subtle_emergences(timestamp, name, state);'
INSERT_STRING = 'INSERT INTO subtle_emergences VALUES (?, ?, ?);'

class Record(object):
    def __init__(self, app):
        self.app = app
        
        # initialize connection
        self.conn = sqlite3.Connection(DB_NAME)
        self.cur = self.conn.cursor()
        
        self.cur.execute(CREATE_STRING)
    
    def update(self):
        ts = datetime.now()
        rows = []
        
        # Pass Sensor Data
        for s in self.app.sensors:
            rows.append([ts, s.name, s.read()])
        
        # Pass Sprite Data
        for s in self.app.sprites:
            rows.append([ts, s.name, s.states.state.name])
            
        self.cur.executemany(INSERT_STRING, rows)