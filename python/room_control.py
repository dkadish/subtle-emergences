#!/usr/local/bin/python

import os, json, random

import pygame
import pygame.locals as locs

from emergences.sprite import StateSprite
from emergences.sprite.leaves import LeafSprite
from emergences.sensor import Sensor
from emergences.record import Record
#from emergences.serial.persistence import connect, map_connections

TICKS = 15
PATH = os.path.dirname(os.path.abspath(__file__))

SONG_END = pygame.USEREVENT + 1
SONG_MU = 10000
SONG_SIGMA = 2000

SERIAL = True
if SERIAL:
    from emergences.serial.arduino import ArduinoConnectionManager as CMananager
else:    
    from emergences.serial import FileConnectionManager as CMananager


import os
#os.environ['SDL_VIDEODRIVER'] = 'dummy'

class App:
    # pylint: disable=too-many-instance-attributes

    def __init__(self):
        print 'Starting Init'
        self._running = True
        self._display_surf = None
        self.size = self.weight, self.height = 640, 400
        
        print 'Loading Connections...'        
        self.connections = CMananager(['4', '3', '5', '6', '7'])
        print self.connections, CMananager
        
        print 'Loading Sprites...'
        self.sensors = []            
        j = json.load(open(os.path.join(PATH, 'config/sensors.json')))
        for sensor in j:
            l = Sensor.from_json(self.connections, j[sensor], sensor)
            self.sensors.append(l)
            
        self.sprites = []
        j = json.load(open(os.path.join(PATH, 'config/leaves.json')))
        for leaf in j:
            l = LeafSprite.from_json(self, j[leaf])
            self.sprites.append(l)
            
        j = json.load(open(os.path.join(PATH, 'config/lights.json')))
        for light in j:
            l = StateSprite.from_json(self, j[light])
            self.sprites.append(l)
        
#        wl = WhiteLeaf(self.connections, '2', 13)
#        self.sprites = [wl]
        
        self.record = Record(self)
            
        print 'Finished Init'

    def on_init(self):
        '''When the room boots up.
        '''
        print 'Starting PyGame Init'
        pygame.init()
        
        # Initialize all of the leaves and lights here
        # Start the audio
        #print os.path.join(PATH, 'audio/nature_sounds.ogg')
        pygame.mixer.music.set_endevent(SONG_END)
        self.audiofiles = [os.path.join(PATH, 'audio/', f) for f in os.listdir(os.path.join(PATH, 'audio/'))]
        self.audiofiles = filter(lambda f: f.split('.')[-1] == 'ogg', self.audiofiles)
        print self.audiofiles        

        self._play_random()
        
        self._running = True
        print 'Finished PyGame Init'
 
    def on_event(self, event):
        '''Handle custom events, such as people entering the room, people
        entering and leaving the zones.
        '''
        if event.type == pygame.QUIT or (event.type == locs.KEYDOWN and event.key == locs.K_ESCAPE):
            self._running = False
                        
        if event.type == SONG_END:
            self._play_random()
            
    def on_loop(self):
        '''Update the parameters for the lights and the leaves (and the sounds).
        '''
        self.connections.update()
        
        # Call update for each leaf and light
        for obj in self.sprites:
            obj.update()
            
        self.record.update()
        
    def on_render(self):
        '''Pass messages on via serial commands to the Arduinos
        '''
        
        for obj in self.sprites:
            obj.on_render()
            
    def on_cleanup(self):
        for obj in self.sprites:
            print 'CLEANUP ON OBJECT: ', obj
            obj.on_cleanup()
            pygame.time.wait(500)
        
        self.connections.close()
            
        pygame.quit()
 
    def on_execute(self):
        if self.on_init() == False:
            self._running = False
            
        self.clock = pygame.time.Clock()
        
        print 'Starting Loop'
        while( self._running ):
            self.clock.tick(15)
            for event in pygame.event.get():
                self.on_event(event)
            self.on_loop()
            self.on_render()
            
            for event in pygame.event.get():
                if event.type == locs.QUIT:
                    return
                elif event.type == locs.KEYDOWN:
                    if event.key == locs.K_ESCAPE:
                        return
        self.on_cleanup()
        
    def _play_random(self):
        f = random.choice(self.audiofiles)
        print 'Changing Song to %s' %f
        pygame.mixer.music.load(f)
        pygame.mixer.music.play()
        
        pygame.time.set_timer(SONG_END, int(random.gauss(SONG_MU, SONG_SIGMA)))
        
if __name__ == "__main__" :
    theApp = App()
    theApp.on_execute()
