# -*- coding: utf-8 -*-
"""
Created on Tue Feb 10 17:17:26 2015

@author: dkadish
"""
import csv
import numpy as np
import scipy
import scipy.interpolate
import joblib

def csv_to_pchip(csv_file, pchip_file):
    f = open(csv_file, 'r')
    c = csv.reader(f)
    xi = []
    yi = []    
    
    for x, y in c:
        xi.append(int(x))
        yi.append(int(y))
        
    pchip = scipy.interpolate.PchipInterpolator(np.array(xi), np.array(yi))
    
    joblib.dump(pchip, pchip_file)
    
if __name__ == '__main__':
    import argparse
    
    parser = argparse.ArgumentParser(description='Convert a comma-separated file to an interpolation object file.')
    parser.add_argument('csv_file')
    parser.add_argument('pchip_file')

    args = parser.parse_args()

    csv_to_pchip(args.csv_file, args.pchip_file)