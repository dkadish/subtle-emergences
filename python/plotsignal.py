import pygame, time, math, csv

from signals import SineWave, SignalGroup

longLoop = SineWave(period = 30*60, v_range = (0.0, 1.0))
midLoop = SineWave(period = 5*60, v_range = (-0.25, 1.0), offset = 20)
shortLoop = SineWave(period = 60, v_range = (-0.5, 1.0), offset = 30)

signal = SignalGroup([longLoop, midLoop, shortLoop], in_range=(0.0, 0.5))

f = open('data.csv', 'w')

with open('data.csv', 'w') as f:
    writer = csv.writer(f, delimiter=',')
    for t in xrange(10*60):
        writer.writerow([t, signal.value(t)])
    
f.close()
