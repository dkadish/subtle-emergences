/* Code for Subtle Emergences
 * 
 * Uses SerialCommand (https://github.com/dkadish/Arduino-SerialCommand) to process serial instructions from a computer.
 * Implements a callback system with a slightly modified version of the original SerialCommand library.
 * See http://awtfy.com/2011/05/23/a-minimal-arduino-library-for-processing-serial-commands/ for an explanation of the
 * original library. The major difference in this version is that you have to pass the serial connection as a variable.
 *  
 * Author: David Kadish
 * 
 */
#if 1
__asm volatile ("nop");
#endif

#define __BLUETOOTH__ 0
#define __XBEE__ 0

#if __BLUETOOTH__
  #define S Serial1
  #define BAUDRATE 112500
#elif __XBEE__
  #define S Serial1
  #define BAUDRATE 9600
#else
  #define S Serial
  #define BAUDRATE 9600
#endif

#define __LUMINOSITY_TSL2561__ 0
#define __HUMD_TEMP_HTU21D__ 0

// Depends on the dkadish version of SerialCommand
// Available at https://github.com/dkadish/Arduino-SerialCommand
#include <SerialCommand.h>
#include <EEPROM.h>

// Both Use Wire.h
#if __LUMINOSITY_TSL2561__ + __HUMD_TEMP_HTU21D__
  #include <Wire.h>
#endif
#if __LUMINOSITY_TSL2561__
  #include <SFE_TSL2561.h>
  
  SFE_TSL2561 light;
  boolean gain;
  unsigned int ms;
#endif
#if __HUMD_TEMP_HTU21D__
  #include "HTU21D.h"
  HTU21D humidity;
#endif

const int pins[] = {3, 5, 6, 9, 10, 11, 13};
const int N = 7;
int board_id;

SerialCommand sCmd(&S); 

boolean systemOn = true;
int ledMode = HIGH;

void setup() {
  // Setup output pins
  for( int i = 0; i < N; i++ ){
    pinMode(pins[i], OUTPUT);
    analogWrite(pins[i],0);
  }
  
  pinMode(13, OUTPUT);
  
  // Setup Serial
  S.begin(BAUDRATE);
  delay(100);

  // Setup callbacks for SerialCommand commands
  sCmd.addCommand("ON",    system_on);          // Turns LED on
  sCmd.addCommand("OFF",   system_off);         // Turns LED off
  sCmd.addCommand("ALL",   all);         // Turns LED off
  sCmd.addCommand("PWM",     processPulseWidthManagement);  // Converts two arguments to integers and echos them back
  sCmd.addCommand("LED",     processLED);  // Converts two arguments to integers and echos them back
  sCmd.addCommand("ID",     processID);  // Returns the ID number of the board
  sCmd.addCommand("ALOG",  processAnalog); // Returns a value from an analog read
  /*sCmd.addCommand("SPI",  processSPI); // Returns a value from an SPI sensor
  sCmd.addCommand("I2C",  processI2C); // Returns a value from an I2C sensor*/
  #if __LUMINOSITY_TSL2561__
    sCmd.addCommand("LUM",  processLuminosity);
    
    light.begin();
    gain = 0;
    unsigned char time = 2;
    light.setTiming(gain, time, ms);
    light.setPowerUp();
  #endif
  #if __HUMD_TEMP_HTU21D__
    sCmd.addCommand("HUM",  processHumidity); // Returns a value from an analog read
    sCmd.addCommand("TEMP",  processTemperature); // Returns a value from an analog read
    
    humidity.begin();
  #endif
  
  sCmd.setDefaultHandler(unrecognized);      // Handler for command that isn't matched  (says "What?")
  //S.println("Ready");
  
  board_id = EEPROM.read(board_id);
}

void loop() {
  sCmd.readSerial();     // We don't do much, just process serial commands
  
  delay(10);
}

void system_on() {
  ///S.println("System on");
  /*for( int i = 0; i < N; i++ ){
    analogWrite(pins[i], 128);
  }*/
  systemOn = true;
}

void system_off() {
  //S.println("System off");
  for( int i = 0; i < N; i++ ){
    analogWrite(pins[i], 0);
  }
  systemOn = false;
}

void all() {
  //S.println("System off");
  for( int i = 0; i < N; i++ ){
    analogWrite(pins[i], 255);
  }
  systemOn = false;
}

void processPulseWidthManagement() {
  //S.println("Got PWM Command");
  int pin, val;
  char *arg;
  
  arg = sCmd.next();
  if( arg != NULL ){
    pin = atoi(arg);
  } else {
    pin = -1;
  }

  arg = sCmd.next();
  if (arg != NULL) {
    val = atoi(arg);    // Converts a char string to an integer
    analogWrite(pin, val);
  }
  else {
    //S.println("No arguments");
  }
}

void processAnalog() {
  //S.println("Got PWM Command");
  int pin;
  char *arg;
  
  arg = sCmd.next();
  if( arg != NULL ){
    pin = atoi(arg);
    if( pin <= 11){
      S.println(analogRead(pin));
    }
  }
}

void processLED(){
  ledMode = !ledMode;
  digitalWrite(13, ledMode);
}

void processID(){
  S.println(board_id);
}

void processLuminosity(){
  #if __LUMINOSITY_TSL2561__
    unsigned int data0, data1;
    
    if (light.getData(data0,data1))
    {
    
      double lux;    // Resulting lux value
      boolean good;  // True if neither sensor is saturated
      
      // Perform lux calculation:
  
      good = light.getLux(gain,ms,data0,data1,lux);
      
      // Print out the results:
      if (good) {
        Serial.println(lux);
      }
    }
  #endif
}

#if __HUMD_TEMP_HTU21D__
void processHumidity(){
  float humd = humidity.readHumidity();
  Serial.println(humd, 1);
}

void processTemperature(){
  float temp = humidity.readTemperature();
  Serial.println(temp, 1);
}
#endif

// This gets set as the default handler, and gets called when no other command matches.
void unrecognized(const char *command) {
  //S.println("What?");
}
